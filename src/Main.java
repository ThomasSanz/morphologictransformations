import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoDump;
import requeterrezo.Resultat;

/**
 * Représente le point d'entrée du programme.
 */
public class Main
{
	/**
	 * Le point d'entrée du programme.
	 * 
	 * @param args Les arguments.
	 */
	public static void main(String[] args)
	{
		System.setProperty("file.encoding","UTF-8");
		
		// Choix du mot
		String term = "";
		Mot word = null;
		boolean isNewFile = true;

		Scanner sc = new Scanner(System.in);

		System.out.println("Appuyez sur \"r\" pour réinitialiser les données enregistrées avant de faire vos requêtes.\nAppuyez sur n'importe quelle autre touche pour passer cette étape.");		
		String response = sc.nextLine();
		
		if (response.equals("r")) {
			resetCache();
		}
		
		while (word == null && isNewFile)
		{
			// Choix d'un terme
			term = chooseTerm(sc);
			
			isNewFile = createFile(term);
			
			if (isNewFile)
			{
				// Tentative de récupération du terme
				word = getWord(term);				
			}
		}		
		
		sc.close();
		
		if (isNewFile)
		{
			saveInFile(word);
		}
		
		printTransformedWords(term);
	}

	/**
	 * Lance le choix d'un terme par l'utilisateur.
	 * @return Le terme choisi.
	 */
	private static String chooseTerm(Scanner sc)
	{

		System.out.println("Veuillez renseigner un mot unique pour lancer la recherche :");
		String term = sc.nextLine().trim().toLowerCase();

		while (term.isBlank() || term.contains(" "))
		{
			System.out.println("Le mot renseigné n'est pas valide. Veuillez renseigner un mot unique pour lancer la recherche :");
			term = sc.nextLine().trim().toLowerCase();
		}

		return term;
	}

	/**
	 * Récupère le terme enregistré dans la base de données de JeuxDeMot.
	 * @param term Le terme.
	 * @return Le terme trouvé dans la base de données de JeuxDeMot.
	 */
	private static Mot getWord(String term)
	{
		try 
		{
			RequeterRezo rezo = new RequeterRezoDump();
			Resultat resultatRequete = rezo.requete(term);
			
			Mot resultedWord = resultatRequete.getMot();
			
			if (resultedWord == null)
			{
				System.out.println("Le terme demandé n'existe pas dans notre base de données.");
			}
			
			return resultedWord;
		}
		catch (Exception e)
		{
			System.out.println("Le terme demandé est mal formatté dans notre base de données.");
			
			File myObj = new File("savedWords\\" + term + ".txt");
        	myObj.delete();
			
			return null;
		}
	}
	
	/**
	 * Crée un fichier s'il n'existe pas déja.
	 * @param term Le terme.
	 * @return <code>true</code> si le fichier a été créé, <code>false</code> sinon.
	 */
	private static boolean createFile(String term)
	{	
		try
		{
			File directory = new File("savedWords");
		    if (! directory.exists())
		    {
		        directory.mkdir();
		    }
			
			// Création du fichier
			File file = new File("savedWords\\" + term + ".txt");
			return file.createNewFile();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Enregistre les transformations trouvées dans un fichier.
	 * @param word Le mot.
	 */
	private static void saveInFile(Mot word)
	{
		// Récupération des transformations
		List<String> transformedWords = Rules.getTransformedWords(word);

		try
		{
			FileWriter writer = new FileWriter("savedWords\\" + word.getNom() + ".txt");
			
			// Enregistrement des transformations
			for (String transformedWord : transformedWords)
			{
				if (!transformedWord.equals(word.getNom()))
				{
					writer.write(transformedWord + "\n");					
				}
			}
			
			writer.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Affiche les transformations trouvées.
	 * @param term Le terme.
	 */
	private static void printTransformedWords(String term)
	{
		try
		{
			// Récupération du fichier
			File file = new File("savedWords\\" + term + ".txt");
			Scanner reader = new Scanner(file);
			
			while (reader.hasNextLine())
			{
				// Lecture des termes enregistrés
				String word = reader.nextLine();
				System.out.println(word);
			}
			
			reader.close();
    	}
		catch (FileNotFoundException e)
		{
    		System.out.println("Une erreur est survenue.");
    		e.printStackTrace();
	    }
	}
	
	/**
	 * Réinitialise le cache des transformations sauvegardées
	 */
	private static void resetCache()
	{
		// Récupération des nom de fichiers
        File file = new File("savedWords");
        String[] pathnames = file.list();
        
        for (String fileName : pathnames)
		{
        	// Suppression des fichiers
        	File myObj = new File("savedWords\\" + fileName);
        	myObj.delete();
		}
	}
}
