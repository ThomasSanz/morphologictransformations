import requeterrezo.Mot;

/**
 * Représente les tests.
 */
public class Tests
{

	/**
	 * Permet de savoir si un mot est un verbe à l'infinitif.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe à l'infinitif,
	 *         <code>false</code> sinon.
	 */
	public static boolean isInfinitiveVerb(Mot word)
	{		
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(4).stream()
				.filter(r -> r.getNomDestination().toLowerCase().startsWith("ver:inf")).count() > 0;
	}

	/**
	 * Permet de savoir si un mot est un verbe.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe, <code>false</code> sinon.
	 */
	public static boolean isVerb(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(4).stream()
				.filter(r -> r.getNomDestination().toLowerCase().startsWith("ver")).count() > 0;
	}

	/**
	 * Permet de savoir si un mot est un verbe du premier groupe.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe du premier groupe,
	 *         <code>false</code> sinon.
	 */
	public static boolean isFirstGroupVerb(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("er");
	}

	/**
	 * Permet de savoir si un mot est un verbe du deuxième groupe.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe du deuxième groupe,
	 *         <code>false</code> sinon.
	 */
	public static boolean isSecondGroupVerb(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(18).stream()
				.filter(r -> r.getNomDestination().toLowerCase().startsWith("ver:groupe2")).count() > 0;
	}

	/**
	 * Permet de savoir si un mot est un verbe du premier groupe terminant par ger.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe du premier groupe terminant par ger,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_ger(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("ger");
	}

	/**
	 * Permet de savoir si un mot est un verbe du premier groupe terminant par ner.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe du premier groupe terminant par ner,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_ner(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("ner");
	}

	/**
	 * Permet de savoir si un mot est un verbe du premier groupe terminant par quer.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe du premier groupe terminant par quer,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_quer(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("quer");
	}

	/**
	 * Permet de savoir si un mot est un verbe se terminant par ir.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un verbe se terminant par ir,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_ir(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("ir");
	}
	
	/**
	 * Permet de savoir si un mot est un nom.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un nom, <code>false</code> sinon.
	 */
	public static boolean isNoun(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(4).stream()
				.filter(r -> r.getNomDestination().toLowerCase().startsWith("nom")).count() > 0;
	}
	
	/**
	 * Permet de savoir si un mot est un participe présent.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un participe présent, <code>false</code> sinon.
	 */
	public static boolean isPresentParticiple(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(4).stream()
				.filter(r -> r.getNomDestination().toLowerCase().startsWith("ver:ppre")).count() > 0;
	}
	
	/**
	 * Permet de savoir si un mot est un lieu.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est un lieu, <code>false</code> sinon.
	 */
	public static boolean isPlace(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(6).stream()
				.filter(r -> r.getNomDestination().toLowerCase().equals("lieu")).count() > 0;
	}
	
	/**
	 * Permet de savoir si un mot est une personne.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est une personne, <code>false</code> sinon.
	 */
	public static boolean isPerson(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(6).stream()
				.filter(r -> r.getNomDestination().toLowerCase().equals("personne")).count() > 0;
	}
	
	/**
	 * Permet de savoir si un mot est une action.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot est une action, <code>false</code> sinon.
	 */
	public static boolean isAction(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getRelationsSortantesTypees(6).stream()
				.filter(r -> r.getNomDestination().toLowerCase().equals("action")).count() > 0;
	}

	/**
	 * Permet de savoir si un mot termine par uire.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par uire,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_uire(Mot word)
	{
		if (word == null) 
		{
			return false;
		}
		
		return word.getNom().endsWith("uire");
	}

	/**
	 * Permet de savoir si un mot termine par age.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par age,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_age(Mot word)
	{
		if (word == null)
		{
			return false;
		}
		
		return word.getNom().endsWith("age");
	}

	/**
	 * Permet de savoir si un mot termine par ant.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par ant,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_ant(Mot word)
	{
		if (word == null)
		{
			return false;
		}
		
		return word.getNom().endsWith("ant");
	}

	/**
	 * Permet de savoir si un mot termine par ante.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par ante,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_ante(Mot word)
	{
		if (word == null)
		{
			return false;
		}
		
		return word.getNom().endsWith("ante");
	}

	/**
	 * Permet de savoir si un mot termine par eur.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par eur,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_eur(Mot word)
	{
		if (word == null)
		{
			return false;
		}
		
		return word.getNom().endsWith("eur");
	}

	/**
	 * Permet de savoir si un mot termine par euse.
	 * 
	 * @param word Le mot.
	 * @return <code>true</code> si le mot termine par euse,
	 *         <code>false</code> sinon.
	 */
	public static boolean isEndingWith_euse(Mot word)
	{
		if (word == null)
		{
			return false;
		}
		
		return word.getNom().endsWith("euse");
	}
}
