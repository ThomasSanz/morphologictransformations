import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import requeterrezo.Mot;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoDump;

/**
 * Représente les règles.s
 */
public class Rules
{
	/**
	 * La liste des résultats des transformations.
	 */
	private static List<String> transformedWords;
	
	private static String unknownAsNoun = " (n'est pas connu comme un nom dans JeuDeMots)";
	private static String unknownAsAction = " (n'est pas connu comme une action dans JeuDeMots)";
	private static String unknownAsPerson = " (n'est pas connu comme une personne dans JeuDeMots)";
	private static String unknownAsPresentParticiple = " (n'est pas connu comme participe présent dans JeuDeMots)";
	private static String unknownAsFirstGroupVerb = " (n'est pas connu comme verbe du premier groupe dans JeuDeMots)";

	public static List<String> getTransformedWords(Mot word)
	{
		if (transformedWords == null)
		{
			transformedWords = new ArrayList<String>();			
		}

		if (Tests.isInfinitiveVerb(word))
		{
			infinitiveVerbRules(word);
		}
		
		if (Tests.isNoun(word))
		{
			nounRules(word);
		}

		List<String> tr = transformedWords.stream().distinct().collect(Collectors.toList());
		return tr;
	}

	private static void infinitiveVerbRules(Mot word)
	{
		if (Tests.isFirstGroupVerb(word))
		{
			infinitiveVerbFirstGroupRules(word);
		}
		else if (Tests.isSecondGroupVerb(word))
		{
			infinitiveVerbSecondGroupRules(word);
		}
		else if (Tests.isEndingWith_uire(word))
		{
			infinitiveVerb_uireRules(word);
		}
		else if (Tests.isEndingWith_ir(word))
		{
			infinitiveVerb_irRules(word);
		}
	}

	private static void infinitiveVerbFirstGroupRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("er$", "age");
		if (!Tests.isAction(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsAction;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "e");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "ement");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "eur");
		if (!Tests.isPerson(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPerson;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "euse");
		if (!Tests.isPerson(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPerson;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "oir");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "oire");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "ation");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "ure");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "ier");
		if (!Tests.isPerson(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPerson;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("er$", "ant");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		if (!Tests.isPresentParticiple(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPresentParticiple;
		}
		transformedWords.add(newWord);

		if (Tests.isEndingWith_ger(word))
		{
			infinitiveVerbFirstGroup_gerRules(word);
		}

		if (Tests.isEndingWith_ner(word))
		{
			infinitiveVerbFirstGroup_nerRules(word);
		}
		
		if (Tests.isEndingWith_quer(word))
		{
			infinitiveVerbFirstGroup_querRules(word);
		}
	}

	private static void infinitiveVerbFirstGroup_gerRules(Mot word)
	{		
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("er$", "eoir");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);			

		newWord = word.getNom().replaceAll("er$", "eoire");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);			
		
		newWord = word.getNom().replaceAll("er$", "eant");
		if (!Tests.isPresentParticiple(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPresentParticiple;
		}
		transformedWords.add(newWord);			
	}

	private static void infinitiveVerbFirstGroup_nerRules(Mot word)
	{		
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("ner$", "naison");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);			
	}

	private static void infinitiveVerbFirstGroup_querRules(Mot word)
	{		
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("quer$", "cation");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);			
	}
	
	private static void infinitiveVerbSecondGroupRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("ir$", "issement");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("ir$", "ition");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("ir$", "issant");
		if (!Tests.isPresentParticiple(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPresentParticiple;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("ir$", "isseur");
		if (!Tests.isPerson(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPerson;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("ir$", "isseuse");
		if (!Tests.isPerson(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPerson;
		}
		transformedWords.add(newWord);
	}
	
	private static void infinitiveVerb_uireRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("uire$", "uction");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("uire$", "ucteur");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);

		newWord = word.getNom().replaceAll("uire$", "uctrice");
		if (!Tests.isNoun(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsNoun;
		}
		transformedWords.add(newWord);
	}
	
	private static void infinitiveVerb_irRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("ir$", "ant");
		if (!Tests.isPresentParticiple(rezo.requete(newWord).getMot()))
		{
			newWord += unknownAsPresentParticiple;
		}
		transformedWords.add(newWord);
	}
	
	private static void nounRules(Mot word)
	{
		if (Tests.isEndingWith_age(word))
		{
			noun_ageRules(word);			
		}
		
		if (Tests.isEndingWith_ant(word))
		{
			noun_antRules(word);
		}
		
		if (Tests.isEndingWith_ante(word))
		{
			noun_anteRules(word);
		}
		
		if (Tests.isEndingWith_eur(word))
		{
			noun_eurRules(word);
		}
		
		if (Tests.isEndingWith_euse(word))
		{
			noun_euseRules(word);
		}
	}
	
	private static void noun_ageRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newWord = word.getNom().replaceAll("age$", "er");
		Mot newVerb = rezo.requete(newWord).getMot();
		if (!Tests.isFirstGroupVerb(newVerb))
		{
			newWord += unknownAsFirstGroupVerb;
		}
		else
		{
			getTransformedWords(newVerb);			
		}
		transformedWords.add(newWord);
		
		newWord = word.getNom().replaceAll("age$", "ager");
		newVerb = rezo.requete(newWord).getMot();
		if (!Tests.isFirstGroupVerb(newVerb))
		{
			newWord += unknownAsFirstGroupVerb;
		}
		else 
		{
			getTransformedWords(newVerb);			
		}
		transformedWords.add(newWord);
	}
	
	private static void noun_antRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newTerm = word.getNom().replaceAll("ant$", "er");
		Mot newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isFirstGroupVerb(newWord))
		{
			newTerm += unknownAsFirstGroupVerb;
		}
		else
		{
			getTransformedWords(newWord);			
		}
		transformedWords.add(newTerm);
		
		newTerm = word.getNom().replaceAll("ant$", "ante");
		newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isNoun(newWord))
		{
			newTerm += unknownAsNoun;
		}
		transformedWords.add(newTerm);
	}

	private static void noun_anteRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newTerm = word.getNom().replaceAll("ante$", "er");
		Mot newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isFirstGroupVerb(newWord))
		{
			newTerm += unknownAsFirstGroupVerb;
		}
		else
		{
			getTransformedWords(newWord);			
		}
		transformedWords.add(newTerm);
		
		newTerm = word.getNom().replaceAll("ante$", "ant");
		newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isNoun(newWord))
		{
			newTerm += unknownAsNoun;
		}
		transformedWords.add(newTerm);
	}
	
	private static void noun_eurRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newTerm = word.getNom().replaceAll("eur$", "er");
		Mot newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isFirstGroupVerb(newWord))
		{
			newTerm += unknownAsFirstGroupVerb;
		}
		else
		{
			getTransformedWords(newWord);			
		}
		transformedWords.add(newTerm);
		
		newTerm = word.getNom().replaceAll("eur$", "euse");
		newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isNoun(newWord))
		{
			newTerm += unknownAsNoun;
		}
		transformedWords.add(newTerm);
	}
	
	private static void noun_euseRules(Mot word)
	{
		RequeterRezo rezo = new RequeterRezoDump();

		String newTerm = word.getNom().replaceAll("euse$", "er");
		Mot newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isFirstGroupVerb(newWord))
		{
			newTerm += unknownAsFirstGroupVerb;
		}
		else
		{
			getTransformedWords(newWord);			
		}
		transformedWords.add(newTerm);
		
		newTerm = word.getNom().replaceAll("euse$", "eur");
		newWord = rezo.requete(newTerm).getMot();
		if (!Tests.isNoun(newWord))
		{
			newTerm += unknownAsNoun;
		}
		transformedWords.add(newTerm);
	}
}
