<h1>Contexte</h1>

Ce projet a été réalisé dans le cadre d'un master 1 informatique AIGLE à la faculté des sciences de Montpellier. Plus particulièrement au cours de l'enseignement sur le traitement automatique du langage naturel.

<h1>Objectifs</h1>

- Générer des transformations grammaticales à partir d'un mot de la langue française gràce au réseau du site <a href='http://www.jeuxdemots.org/jdm-accueil.php'>JeuxDeMots</a>.

- Les transformations suivent le modèle suivant :

   _**manger -> mangeur, mangeuse, mangeant, mangeoire**_

   _**dormir -> dormeur, dormeuse, dormant**_

   _**marcher -> marche, marchant, marchande, marcheur, marcheuse**_

<h1>Code</h1>

- Codé en Java.

- Utilise la librairie <a href='https://gite.lirmm.fr/benoits/requeterrezo.git'>Requeterrezo</a> pour accéder au réseau de JeuxDeMots.

- Application en ligne de commande.

<h1>Exécution</h1>

1. Ouvrir un terminal dans le dossier principal nommé `morphologictransformations-master`.

2. Exécuter la commande `java -Dfile.encoding=UTF-8 -jar Transformations.jar`.

